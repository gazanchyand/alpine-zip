# Alpine ZIP

Stolen from [Kramos](https://github.com/kramos/alpine-zip) for update purposes

## Quickstart

```
docker run -v <local path to zip>:/to_zip zip -r <zip file name> /to_zip
```
